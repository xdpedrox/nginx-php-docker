FROM php:7-fpm

RUN apt-get update && apt-get install -y  memcached libldb-dev libzip-dev libbz2-dev curl libxml2-dev libcurl4-gnutls-dev firebird-dev libssl-dev libexpat1-dev gettext libfreetype6-dev libjpeg62-turbo-dev ffmpeg libpng-dev libc-client-dev libkrb5-dev \
        git \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libmcrypt-dev \
        libssl-dev \
        libmemcached-dev \
        libz-dev \
        zlib1g-dev \
        libsqlite3-dev \
        zip \
        libedit-dev \
        libpspell-dev \
        libldap2-dev \
        unixodbc-dev \
        libpq-dev \
        sudo \
        procps \
         && rm -r /var/lib/apt/lists/*
RUN docker-php-ext-configure imap --with-kerberos --with-imap-ssl && docker-php-ext-install imap

RUN docker-php-ext-install mysqli zend_test zip ldap bcmath bz2 calendar dba exif ftp gd gettext intl opcache pdo pdo_mysql pspell sockets 

RUN useradd -m phpgg --uid=1001
