# Base system is the LTS version of Ubuntu.
FROM   ubuntu:18.04

ENV DEBIAN_FRONTEND=noninteractive

# Make sure we don't get notifications we can't answer during building.

RUN apt-get update && apt-get install -y software-properties-common && apt-get update

# Download and install everything from the repos.
RUN add-apt-repository universe
## Because people may have trouble installing the php-fpm. But this work around worked for my install.

RUN apt-get update

RUN apt install -y openssl curl nginx mysql-client mysql-server php-fpm php-cli php-curl php-mysql

# # Load in all script files.
# ADD config/pufferpanel/scripts/start.sh /
# ADD config/pufferpanel/scripts/install.exp /srv/install

# Load in all script files.
COPY ./config/pufferpanel/scripts/start.sh /start
COPY ./config/pufferpanel/scripts/install.exp /srv/install

# Download PufferPannel

RUN    mkdir -p /srv && cd /srv && \
       curl -L -o pufferpanel.tar.gz https://github.com/PufferPanel/PufferPanel/releases/download/v1.2.4/pufferpanel.tar.gz && \
       tar -xf pufferpanel.tar.gz && \
       cd pufferpanel  && \
       chmod +x pufferpanel

# Install Pufferd
#RUN    curl -s https://packagecloud.io/install/repositories/pufferpanel/pufferd/script.deb.sh | bash && \
#       apt install pufferd


# Fix all permissions
RUN    chmod +x /start
RUN    chmod +x /srv/install

# Configure Nginx
RUN    rm /etc/nginx/sites-enabled/default

# /start runs it.
CMD    ["/start"]