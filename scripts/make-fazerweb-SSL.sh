#!/bin/bash

certbot certonly --standalone -d cloud.fazerweb.com \
    -d channel.fazerweb.com \
	-d phpmyadmin.fazerweb.com \
	-d rank.eliteportuguesa.pt \
	-d channeldeleter.eliteportuguesa.pt \
	-d bans.eliteportuguesa.pt \
	-d gp.eliteportuguesa.pt \
	-d dynmap.eliteportuguesa.pt \
	-d files.fazerweb.com \
	-d www.eliteportuguesa.pt,eliteportuguesa.pt 
